﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace LastHope
{
    class LaserTurret : Turret
    {
        public LaserTurret( Mothership parent, Sprite spriteBase, Sprite spriteGun)
            : base( 10, 15, 100, 120, 0.2f, parent, spriteBase, spriteGun )
        {
            Texture2D tex = parent.parent.Content.Load<Texture2D>(@"Texture\c-240-240");
            //RangeSprite = new CenteredSprite(spriteBase.Center, tex, new Rectangle(0, 0, range, range), new Vector2(0, 0));
            RangeSprite = new CenteredSprite(spriteBase.Center, tex, new Rectangle(0, 0, 260, 260), new Vector2(0, 0));}

        public override void shoot(Enemy enemy, int dmg, double rotation)
        {
            Vector2 addition = new Vector2(-(float)Math.Sin(rotation) * 20, (float)Math.Cos(rotation) * 20);
            Texture2D spriteImage = parent.parent.Content.Load<Texture2D>(@"Texture/laser-ray"); // depends
            Sprite sprite = new CenteredSprite(spriteBase.Center + addition, spriteImage, new Rectangle(0, 0, 10, 10), new Vector2(0, 0) );

            Projectile p = new Projectile(dmg, enemy, sprite, 350.0f, parent.ProjectileMgr);
            SoundManager.PlayLazor();
        }
    }
}
