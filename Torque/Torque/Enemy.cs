﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LastHope
{
    public class Enemy : Drawable, Updatable 
    {
        const float ARMOR_REDUCTION_COEFF = 0.06f;

        public Sprite sprite { get; private set; }

        public int hitpoints
        {
            get;
            private set;
        }

        public int armor 
        {
            get;
            private set;
        }

        public int bounty
        {
            get;
            private set;
        }

        public WaveManager waveMgr { get; private set; }

        public Vector2 HBarDisplacement = new Vector2(0, -5);

        private int maxhitpoints;

        public Enemy( int _hitpoints, int _armor, int _bounty, int _speed, WaveManager _waveMgr, Vector2 spawnLocation, Vector2 initialVelocity, Texture2D texture)
        {
            maxhitpoints = hitpoints = _hitpoints;
            armor = _armor;
            waveMgr = _waveMgr;
            bounty = _bounty;
            //Enemy spawn
            Rectangle firstFrame = new Rectangle(0,0, 40, 40);
            sprite = new CenteredSprite(spawnLocation, texture, firstFrame, initialVelocity);

            //Todo fix:
            Speed = _speed;

            Texture2D hbtex = waveMgr.parent.parent.Content.Load<Texture2D>(@"Texture\Health");
            HealthBar = new Sprite(sprite.Location + HBarDisplacement, hbtex, new Rectangle(0, 0, 3, 3), new Vector2(0, 0));
        }

        public bool HealthBarVisible = false;

        void Updatable.Update(GameTime time)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.LeftAlt) || Keyboard.GetState().IsKeyDown(Keys.RightAlt))
                HealthBarVisible = true;
            else
                HealthBarVisible = false;

            HealthBar.Location = sprite.Location + HBarDisplacement;

            if (CurrentWaypoint == null)
                CurrentWaypoint = Path.GetNextWaypoint();
            else
            {
                // check for core colision
                if (Vector2.DistanceSquared(waveMgr.parent.Sprite.Center, this.Position) <= 8000.0)
                {
                    SoundManager.PlayWarning();

                    hitpoints = 0;
                    // kill me
                    waveMgr.kill(this);
                    // reduce life counter
                    waveMgr.parent.decrementLife();
                    return;
                }

                if (Vector2.DistanceSquared(CurrentWaypoint.Position, this.Position) <= (Speed / 60) * (Speed / 60))
                {
                    CurrentWaypoint = Path.GetNextWaypoint();
                    ++EatenWaypoints;
                }
                else
                {
                    if (CurrentWaypoint.Type == WaypointType.STRAIGHT)
                    {
                        sprite.Velocity = (CurrentWaypoint.Position - this.Position);
                        sprite.Velocity = Vector2.Normalize(sprite.Velocity);
                        sprite.Velocity *= Speed;
                        sprite.Rotation = (float)Polar.VecToAngle((CurrentWaypoint.Position - sprite.Center));
                    }
                    else if (CurrentWaypoint.Type == WaypointType.CIRCULAR)
                    {
                        Mothership mothership = waveMgr.parent;
                        int i = (EatenWaypoints)/ 2 - 1;

                        if (i >= 0 && i < mothership.ActiveWallsList.Count)
                        {
                            Vector2 curr_wall_pos = mothership.ActiveWallsList[i].Sprite.Center - mothership.Sprite.Center;
                            double wall_angle = Polar.VecToAngle(curr_wall_pos);
                            Vector2 curr_relative_pos = sprite.Center - mothership.Sprite.Center;
                            double curr_angle = Polar.VecToAngle(curr_relative_pos);
                            double door_angle = Polar.VecToAngle(Path.PeekNextWaypoint().Position - mothership.Sprite.Center);

                            double deltaw = wall_angle - curr_angle;
                            double deltad = door_angle - curr_angle;

                            CircularDirection direction;
                            if ((deltaw > 0 && deltad > 0) || (deltad < 0 && deltaw < 0))
                            {
                                if (Math.Abs(deltaw) < Math.Abs(deltad))
                                {
                                    if (deltaw > 0)
                                        direction = CircularDirection.COUNTER_CLOCKWISE;
                                    else
                                        direction = CircularDirection.CLOCKWISE;
                                }
                                else
                                {
                                    if (deltaw < 0)
                                        direction = CircularDirection.COUNTER_CLOCKWISE;
                                    else
                                        direction = CircularDirection.CLOCKWISE;
                                }
                            }
                            else
                            {
                                if (deltaw < 0)
                                    direction = CircularDirection.CLOCKWISE;
                                else
                                    direction = CircularDirection.COUNTER_CLOCKWISE;
                            }

                            CurrentWaypoint.Direction = direction;
                        }
                        //////////////////////////////
                        Vector2 temp = waveMgr.parent.Position - this.Position;
                        temp.Normalize();
                        Vector2 dir = -temp;
                        if (CurrentWaypoint.Direction == CircularDirection.COUNTER_CLOCKWISE)
                        {
                            temp.Y = -temp.Y;
                            float t = temp.Y;
                            temp.Y = temp.X;
                            temp.X = t;
                        }
                        else if (CurrentWaypoint.Direction == CircularDirection.CLOCKWISE)
                        {
                            temp.X = -temp.X;
                            float t = temp.Y;
                            temp.Y = temp.X;
                            temp.X = t;
                        }

                        temp *= Speed;
                        sprite.Velocity = temp;
                        sprite.Rotation = (float)Polar.VecToAngle(sprite.Velocity);

                        temp = waveMgr.parent.Position + dir * Vector2.Distance(waveMgr.parent.Position, CurrentWaypoint.Position);
                        ((CenteredSprite)sprite).Center = temp;
                    }
                }
            }

            sprite.Update(time);
        }

        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);

            if (HealthBarVisible)
            {
                float width = sprite.Destination.Width/3;
                width = width * (float)hitpoints / (float)maxhitpoints;

                spriteBatch.Draw(HealthBar.Texture, HealthBar.Location, new Rectangle(0, 0, 3, 3), Color.Lerp(Color.Red, Color.Green, (float)hitpoints / (float)maxhitpoints), 0, new Vector2(0, 0), new Vector2(width, 1), SpriteEffects.None, 0);
            }
        }

        private void _recalculate_full_path(Mothership mothership)
        {
            Path.Clear();

            for (int i = 0; i < mothership.DoorsList.Count; ++i)
            {
                WaypointType type = WaypointType.STRAIGHT;
                CircularDirection direction = CircularDirection.COUNTER_CLOCKWISE;

                if (i != 0)
                {
                    type = WaypointType.CIRCULAR;
                    //if (i != mothership.DoorsList.Count)
                    //{
                    //    Vector2 curr_wall_pos = mothership.ActiveWallsList[i - 1].Sprite.Center - mothership.Sprite.Center;
                    //    double wall_angle = Polar.VecToAngle(curr_wall_pos);
                    //    Vector2 curr_relative_pos = Path.Last().Position - mothership.Sprite.Center;
                    //    double curr_angle = Polar.VecToAngle(curr_relative_pos);
                    //    double door_angle = Polar.VecToAngle(mothership.DoorsList[i].Position - mothership.Sprite.Center);

                    //    double deltaw = wall_angle - curr_angle;
                    //    double deltad = door_angle - curr_angle;

                    //    if ((deltaw > 0 && deltad > 0) || (deltad < 0 && deltaw < 0))
                    //    {
                    //        if (Math.Abs(deltaw) < Math.Abs(deltad))
                    //        {
                    //            if (deltaw > 0)
                    //                direction = CircularDirection.COUNTER_CLOCKWISE;
                    //            else
                    //                direction = CircularDirection.CLOCKWISE;
                    //        }
                    //        else
                    //        {
                    //            if (deltaw < 0)
                    //                direction = CircularDirection.COUNTER_CLOCKWISE;
                    //            else
                    //                direction = CircularDirection.CLOCKWISE;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (deltaw < 0)
                    //            direction = CircularDirection.CLOCKWISE;
                    //        else
                    //            direction = CircularDirection.COUNTER_CLOCKWISE;
                    //    }
                    //}
                }

                Path.AddWaypoint(new Waypoint(type, mothership.DoorsList[i].Position - mothership.DoorsList[i].Orientation * Mothership.RING_WIDTH, direction));
                Path.AddWaypoint(new Waypoint(WaypointType.STRAIGHT, mothership.DoorsList[i].Position + mothership.DoorsList[i].Orientation * Mothership.RING_WIDTH));
            }

            Path.AddWaypoint(new Waypoint(WaypointType.STRAIGHT, mothership.Position)); // go to the braiiin
        }

        public void RecalculatePath(Mothership mothership)
        {
            if (CurrentWaypoint == null)
            {
                _recalculate_full_path(mothership);
            }
            else
            {
                _recalculate_full_path(mothership);
                Path.RemoveFirst(EatenWaypoints);
            }
        }

        private Path Path = new Path();
        private int CurrentLevel = 0; // trenutni nivo po prstenovima
        private int EatenWaypoints = 0;
        private Waypoint CurrentWaypoint = null; 

        public int dealDamage(int totalDamage)
        {
            int damageTook = reduceDamage( totalDamage );
            hitpoints -= damageTook;


            if (0 >= hitpoints)
            {
                hitpoints = 0;
                // gimmeh moneyz!
                waveMgr.parent.addEnergy(bounty);
                // kill me
                waveMgr.kill(this);

            }

            // show damage?

            return damageTook;
        }

        // dmg * (1 - armorReduction)
        // armorReduction = COEFF * armor / (1 + COEFF * armor)
        protected int reduceDamage( int dmg )
        {
            return (int)Math.Round(dmg * (1.0f - ARMOR_REDUCTION_COEFF * armor / (1 + ARMOR_REDUCTION_COEFF * armor)));
        }

        public Vector2 Position
        {
            get
            {
                return sprite.Center;
            }
        }

        public float Speed
        {
            get;
            private set;
        }

        public Sprite HealthBar;
    }
}
