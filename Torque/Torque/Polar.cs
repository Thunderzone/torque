﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
namespace LastHope
{
    static class Polar
    {
        public static double VecToAngle(Vector2 vec)
        {
            if (vec.Y == 0)
                return 0;
            return Math.Atan2(-vec.X , vec.Y);
        }

        public static Vector2 AngleToVec(double angle)
        {
            Vector2 temp = new Vector2(0, 1);
            float cs = (float)Math.Cos(angle);
            float sn = (float)Math.Sin(angle);
            return new Vector2(-sn, cs); // TODO: sredi ovo
        }
    }
}
