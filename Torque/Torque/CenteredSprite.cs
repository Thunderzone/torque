﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework; // mora da se doda ručno
using Microsoft.Xna.Framework.Graphics; // mora da se doda ručno

namespace LastHope
{
    class CenteredSprite : Sprite
    {
        public CenteredSprite(
            Vector2 initialLocation,
            Texture2D texture,
            Rectangle initialFrame,
            Vector2 velocity) : base(initialLocation - (new Vector2(initialFrame.Right - initialFrame.Left, initialFrame.Bottom - initialFrame.Top)) / 2, texture, initialFrame, velocity)
        {
            

        }

        public Vector2 Center
        {
            get
            {
                return base.Center;
            }

            set
            {
                base.Location = value - new Vector2(base.Destination.Height / 2, base.Destination.Width / 2);
            }
        }
    }
}
