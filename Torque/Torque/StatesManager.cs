﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    public class StatesManager
    {
        Game1 game;
        Rectangle startRec;
        Rectangle quitRec;
        Rectangle creditsRec;
        Rectangle creditsBackRec;
        Rectangle gameOverRec;
        Rectangle instructionRec;
        Rectangle instrRecStart;

        Sprite startScreen;
        Sprite credits;
        Sprite gameOver;
        Sprite instr;

        public StatesManager(Game1 game)
        {
            this.game = game;

            startRec = new Rectangle(333, 608, 205, 62);
            quitRec = new Rectangle(1091, 934, 168, 60);
            creditsRec = new Rectangle(56, 934, 300, 60);
            creditsBackRec = new Rectangle(1088, 708, 180, 67);
            gameOverRec = new Rectangle(630, 50, 630, 60);
            instructionRec = new Rectangle(1100, 680, 160, 70);
            instrRecStart = new Rectangle(412, 840, 506, 73);


            Texture2D startTex =  game.Content.Load<Texture2D>(@"Texture/startScreen");
            startScreen = new Sprite(new Vector2(0, 0), startTex, new Rectangle(0, 0, 1280, 1024), new Vector2(0, 0));

            Texture2D creditsTex = game.Content.Load<Texture2D>(@"Texture/credits");
            credits = new Sprite(new Vector2(0, 0), creditsTex, new Rectangle(0, 0, 1280, 1024), new Vector2(0, 0));

            Texture2D gover = game.Content.Load<Texture2D>(@"Texture/GameOver");
            gameOver = new Sprite(new Vector2(0, 0), gover, new Rectangle(0, 0, 1280, 1024), new Vector2(0, 0));

            Texture2D inst= game.Content.Load<Texture2D>(@"Texture/instructions");
            instr = new Sprite(new Vector2(0, 0), inst, new Rectangle(0, 0, 1280, 1024), new Vector2(0, 0));
        }

        public void Update()
        {
            MouseState mouseState = Mouse.GetState();
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                switch (game.currentGameState)
                {
                    case Game1.GameStates.StartScreen:
                        if (startRec.Contains(mouseState.X, mouseState.Y))
                        {
                            game.currentGameState = Game1.GameStates.Playing;
                        }

                        if (quitRec.Contains(mouseState.X, mouseState.Y))
                        {
                            game.currentGameState = Game1.GameStates.Quit;
                        }

                        if (creditsRec.Contains(mouseState.X, mouseState.Y))
                        {
                            game.currentGameState = Game1.GameStates.Credits;
                        }

                        if (instrRecStart.Contains(mouseState.X, mouseState.Y))
                        {
                            game.currentGameState = Game1.GameStates.Instructions;
                        }
                        break;

                    case Game1.GameStates.Credits:
                        if (creditsBackRec.Contains(mouseState.X, mouseState.Y))
                        {
                            game.currentGameState = Game1.GameStates.StartScreen;
                        }
                        break;

                    case Game1.GameStates.GameOver:
                       if (gameOverRec.Contains(mouseState.X, mouseState.Y))
                        {
                            game.currentGameState = Game1.GameStates.StartScreen;
                        }
                        break;

                    case Game1.GameStates.Instructions:
                        if (instructionRec.Contains(mouseState.X, mouseState.Y))
                        {
                            game.currentGameState = Game1.GameStates.StartScreen;
                        }
                        break;
                }


            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (game.currentGameState == Game1.GameStates.StartScreen)
                startScreen.Draw(spriteBatch);

            if (game.currentGameState == Game1.GameStates.Credits)
                credits.Draw(spriteBatch);

            if (game.currentGameState == Game1.GameStates.GameOver)
                gameOver.Draw(spriteBatch);

            if (game.currentGameState == Game1.GameStates.Instructions)
                instr.Draw(spriteBatch);
        }
    }
}
