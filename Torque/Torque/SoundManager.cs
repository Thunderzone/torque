﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    static class SoundManager
    {
        private static List<SoundEffect> explosions = new List<SoundEffect>();
        private static int explosionCount = 8;

        private static SoundEffect Lazor;

        private static Random rand = new Random();
        private static Song Theme;

        private static SoundEffect YouWin;
        private static SoundEffect Gameover;
        private static SoundEffect Prepare;
        private static SoundEffect Cntdown;
        
        private static SoundEffect SoundEffectMissileLaunch;

        private static SoundEffect GatlingGun;
        private static SoundEffect Warning;

        public static void PlayWarning()
        {
            WarningInst.Play();
        }

        public static void PlayYouwin()
        {
            YouWin.Play();
        }

        public static void PlayGameover()
        {
            Gameover.Play();
        }

        public static void PlayPrepare()
        {
            Prepare.Play();
        }

        public static void PlayCntdown()
        {
            Cntdown.Play();
        }

        public static void PlayMissileLaunch()
        {
            SoundEffectInstance minst = SoundEffectMissileLaunch.CreateInstance();
            minst.Volume = 0.15F;
            minst.Play();
        }

        private static SoundEffectInstance WarningInst;

        public static void Initialize(ContentManager content)
        {
            Lazor = content.Load<SoundEffect>(@"Sounds\pshju");
            Theme = content.Load<Song>(@"Sounds\theme");

            YouWin = content.Load<SoundEffect>(@"Sounds\youwin");
            Gameover = content.Load<SoundEffect>(@"Sounds\gameover");
            Prepare = content.Load<SoundEffect>(@"Sounds\prepare");
            Cntdown = content.Load<SoundEffect>(@"Sounds\321");
            SoundEffectMissileLaunch = content.Load<SoundEffect>(@"Sounds\missile_launch");
            GatlingGun = content.Load<SoundEffect>(@"Sounds\gatling");
            Warning = content.Load<SoundEffect>(@"Sounds\warning");
            WarningInst = Warning.CreateInstance();
            WarningInst.Volume = 1.0F;
            //WarningInst.
            for (int x = 1; x <= explosionCount; x++)
            {
                explosions.Add(
                    content.Load<SoundEffect>(@"Sounds\Explosion" +
                        x.ToString()));
            }


            MediaPlayer.Volume = 1.0F;
            MediaPlayer.IsRepeating = true;
            PlayTheme();
        }

        public static void PlayLazor()
        {
            SoundEffectInstance linst = Lazor.CreateInstance();
            linst.Volume = 0.15F;
            linst.Play();
        }


        public static void PlayExplosion()
        {
            explosions[rand.Next(0, explosionCount)].Play();
        }

        public static void PlayExplosion(int n)
        {
            explosions[n].Play();
        }

        public static void PlayTheme()
        {
            MediaPlayer.Play(Theme);
        }

        public static void PlayGatlingShot()
        {
            SoundEffectInstance Ginst = GatlingGun.CreateInstance();
            Ginst.Volume = 0.15F;
            Ginst.Play();
        }
    }
}
