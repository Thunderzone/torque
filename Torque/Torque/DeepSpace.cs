﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{

    class DeepSpace : Drawable, Updatable
    {
        Game game;

        Sprite backgroundSprite;

        float with = 2048.0f;
        float height = 1280.0f;

        private int time = 0;

        private Sprite nebula;

        public DeepSpace(Game game)
        {
            this.game = game;
            Texture2D backgroundTexture = game.Content.Load<Texture2D>(@"Texture/background-big");
            Texture2D nebulaTex = game.Content.Load<Texture2D>(@"Texture/nebula");

            backgroundSprite = new Sprite(Vector2.Zero, backgroundTexture, new Rectangle(0, 0, (int)with, (int)height), new Vector2(-10.0f, -10.0f));

            nebula = new Sprite(new Vector2(250.0f, 250.0f), nebulaTex, new Rectangle(0, 0, 325, 325), new Vector2(1.0f,1.0f));
            //backgroundViewport = new Rectangle(0,0,1024,768);
        }

        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            //spriteBatch.Draw(backgroundTexture, new Rectangle(0, 0, 1024, 768), backgroundViewport, Color.White);

            backgroundSprite.Draw(spriteBatch);

            nebula.Draw(spriteBatch);

        }

        void Updatable.Update(GameTime gameTime)
        {


            if (backgroundSprite.Location.X > 0 || backgroundSprite.Location.X < -(with - game.GraphicsDevice.DisplayMode.Width))
            {
                backgroundSprite.Velocity = new Vector2(-backgroundSprite.Velocity.X, backgroundSprite.Velocity.Y);
            }

            if (backgroundSprite.Location.Y > 0 || backgroundSprite.Location.Y < -(height - game.GraphicsDevice.DisplayMode.Height))
            {
                backgroundSprite.Velocity = new Vector2(backgroundSprite.Velocity.X, -backgroundSprite.Velocity.Y);
            }

            Rectangle rect = new Rectangle(0, 0, game.GraphicsDevice.DisplayMode.Width, game.GraphicsDevice.DisplayMode.Height);
            if (! rect.Contains(nebula.Destination))
            {
                nebula.Velocity = -nebula.Velocity;
            }
            

            backgroundSprite.Update(gameTime);

            nebula.Rotation += MathHelper.Pi / 300000 * gameTime.ElapsedGameTime.Milliseconds;
            nebula.Update(gameTime);
        }

    }
}
