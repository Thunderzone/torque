﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastHope
{
    public class Door : Updatable, Drawable
    {
        void Updatable.Update(GameTime time)
        {

        }

        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            Sprite.Draw(spriteBatch);
        }

        public bool Opened { get; private set; }
        public Vector2 Position { get { return Sprite.Center; } } 
        public Vector2 Orientation
        {
            private set { }
            get
            {
                return Polar.AngleToVec(Sprite.Rotation);
            }
        }

        private Sprite Sprite;

        // pozicija relativna u odnosu na gornji levi ugao sprajta mothershit
        public Door(Vector2 pos, Mothership parent)
        {
            // stvori madafaka sprajt
            Texture2D spriteImage = parent.parent.Content.Load<Texture2D>(@"Texture/ship");
            Vector2 position = pos + parent.Sprite.Location;
            Sprite = new CenteredSprite(position, spriteImage, new Rectangle(0, 0, 40, 40), new Vector2(0, 0));
            Sprite.Rotation = (float)Polar.VecToAngle((parent.Sprite.Center - Sprite.Center));
        }
    }
}
