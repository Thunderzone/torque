﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    class Info
    {
        private Game1 game;

        private List<TextLabel> labels = new List<TextLabel>();

        private Sprite sprite;

        private bool isVisible = false;

        public Info(Game game, Vector2 position)
        {
            this.game = (Game1)game;

            Texture2D texture = game.Content.Load<Texture2D>(@"Texture/info");
            sprite = new Sprite(position, texture, new Rectangle(0, 0, 250, 100), Vector2.Zero);

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (isVisible)
            {
                sprite.Draw(spriteBatch);
            }
        }

        public void Show(TurretButton.TurretType type)
        {
            if (!isVisible)
            {

                switch (type)
                {
                    case TurretButton.TurretType.Aura:
                        TextLabel aName = new TextLabel(@"Aura Tower", new Vector2(200.0f, 30.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel aCost = new TextLabel(@"Cost: 5000 energy", new Vector2(200.0f, 45.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel aRange = new TextLabel(@"AOE Radius: 100 ", new Vector2(200.0f, 60.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        //TextLabel aRange = new TextLabel(@"Range: ", new Vector2(200.0f, 45.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        labels.Add(aCost);
                        labels.Add(aRange);
                        labels.Add(aName);
                        break;
                    case TurretButton.TurretType.Catlling:
                        TextLabel cName = new TextLabel(@"Gatlling Tower ", new Vector2(200.0f, 30.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel cCost = new TextLabel(@"Cost: 1600 energy ", new Vector2(200.0f, 45.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel cRange = new TextLabel(@"Range: 300 ", new Vector2(200.0f, 60.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel cDmg = new TextLabel(@"Damage: 10 - 30 ", new Vector2(200.0f, 75.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel cRs = new TextLabel(@"Rotation Speed: 0.3 ", new Vector2(200.0f, 90.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);

                        labels.Add(cName);
                        labels.Add(cCost);
                        labels.Add(cRange);
                        labels.Add(cDmg);
                        labels.Add(cRs);
                        break;
                    case TurretButton.TurretType.Laser:
                        TextLabel lName = new TextLabel(@"Laser Tower", new Vector2(200.0f, 30.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel lCost = new TextLabel(@"Cost: 2400 energy", new Vector2(200.0f, 45.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel lRange = new TextLabel(@"Range: 120 ", new Vector2(200.0f, 60.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel lDmg = new TextLabel(@"Damage: 10 - 15 ", new Vector2(200.0f, 75.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel lFr = new TextLabel(@"Firing Rate: 100 ", new Vector2(200.0f, 90.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);

                        labels.Add(lName);
                        labels.Add(lCost);
                        labels.Add(lRange);
                        labels.Add(lDmg);
                        labels.Add(lFr);
                        break;
                    case TurretButton.TurretType.Missile:
                        TextLabel mName = new TextLabel(@"Missile Tower ", new Vector2(200.0f, 30.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel mCost = new TextLabel(@"Cost: 4000 energy", new Vector2(200.0f, 45.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel mRange = new TextLabel(@"Range: 200", new Vector2(200.0f, 60.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);
                        TextLabel mFr = new TextLabel(@"Firing Rate: 2000", new Vector2(200.0f, 75.0f), Color.Wheat, game.textOverlay.sf, game.textOverlay);

                        labels.Add(mName);
                        labels.Add(mCost);
                        labels.Add(mRange);
                        labels.Add(mFr);
                        break;
                }
            }
            isVisible = true;
        }

        public void Hide()
        {
            if (isVisible)
            {
                foreach (TextLabel label in labels)
                {
                    game.textOverlay.removeText(label);
                }
                labels.Clear();

                isVisible = false;
            }
        }

    }
}
