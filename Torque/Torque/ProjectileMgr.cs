﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastHope
{
    public class ProjectileMgr : Updatable, Drawable
    {
        private List<Projectile> projectileList;
        private List<Projectile> deletionList;

        public Mothership Mothership {get; private set;}

        public ProjectileMgr(Mothership motherShip)
        {
            projectileList = new List<Projectile>();
            deletionList = new List<Projectile>();

            Mothership = motherShip;
        }

        public void addProjectile(Projectile p)
        {
            projectileList.Add(p);
        }

        public void destroyProjectile(Projectile p)
        {
            deletionList.Add(p);
        }

        void Updatable.Update(GameTime time)
        {
            foreach (Projectile p in projectileList)
            {
                ((Updatable)p).Update( time );
            }

            foreach (Projectile p in deletionList)
                projectileList.Remove(p);

            deletionList.Clear();
        }

        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            foreach (Projectile p in projectileList)
            {
                ((Drawable)p).Draw(spriteBatch);
            }
        }
    }
}
