﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    class TurretButton: Button
    {



        private TextLabel levelLabel;
        private int level = 1;

        public enum TurretType
        {
            None, Missile, Laser, Catlling, Aura
        }

        private TurretType type;

        private float radius;

        public TurretType Type
        {
            get { return type; }
            set { type = value; }
        }

        public TurretButton(Game1 game, Rectangle rectangle,Texture2D texture, float radius, TurretType type)
            : base(rectangle,texture)
            {
                this.type = type; 
                this.radius = radius;
                //levelLabel = new TextLabel(level.ToString(), new Vector2(rectangle.X, rectangle.Y), Color.White, game.textOverlay.pericles36, game.textOverlay);
            }

        public override bool IsOver(Point point)
        {
            Vector2 cursorPosition = new Vector2(point.X, point.Y);
            if (Vector2.Distance(current.Center, cursorPosition) < radius) return true;
            else return false;
        }

        public int Level
        {
            get { return level; }
            set { level = value; }
        }
    }
}
