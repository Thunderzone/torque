﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace LastHope
{
    enum WaypointType { CIRCULAR, STRAIGHT };
    enum CircularDirection { CLOCKWISE, COUNTER_CLOCKWISE };

    class Waypoint
    {
        public WaypointType Type;
        public Vector2 Position;
        public CircularDirection Direction; // za cirkularni waypoint, da li je direction retrogradan ili ne;

        public Waypoint(WaypointType type, Vector2 position, CircularDirection direction = CircularDirection.CLOCKWISE)
        {
            Type = type;
            Position = position;
            Direction = direction;
        }
    }
}
