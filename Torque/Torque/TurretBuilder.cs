﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    class TurretBuilder
    {

      Button towerBtn;
      Texture2D normalTex;
      Texture2D redTex;
      Texture2D greenTex;
      Texture2D rangeTex;
      
      Texture2D baseTex;

        Game1 game;

        Sprite selected;
        Sprite overlayRed;
        Sprite overlayGreen;
        Sprite range;
        

        Texture2D fire;



        bool canBuild;

        public TurretBuilder(Game1 game, TurretButton.TurretType type)
        {
            this.game = game;
            Type = type;

            Texture2D greenOvText = game.Content.Load<Texture2D>(@"Texture/green");
            Texture2D redOvText = game.Content.Load<Texture2D>(@"Texture/red");
            overlayGreen = new Sprite(new Vector2(30.0f, 30.0f), greenOvText, new Rectangle(0, 0, 40, 40), Vector2.Zero);
            overlayRed = new Sprite(new Vector2(30.0f, 30.0f), redOvText, new Rectangle(0, 0, 40, 40), Vector2.Zero);

            canBuild = false;

            switch (type)
            {
                case TurretButton.TurretType.Laser:
                    normalTex = game.Content.Load<Texture2D>(@"Texture/laser_normal");
     //               greenTex = game.Content.Load<Texture2D>(@"Texture/laser_green");
 //                   redTex = game.Content.Load<Texture2D>(@"Texture/laser_red");
                    range = new CenteredSprite( new Vector2(0,0), game.Content.Load<Texture2D>(@"Texture\c-240-240"), new Rectangle(0, 0, 260, 260), new Vector2(0, 0));
                    break;

                case TurretButton.TurretType.Catlling:
                    normalTex = game.Content.Load<Texture2D>(@"Texture/gantlling");
                    fire = game.Content.Load<Texture2D>(@"Texture/gatelling-fire");
                    range = new CenteredSprite(new Vector2(0, 0), game.Content.Load<Texture2D>(@"Texture\c-600-600"), new Rectangle(0, 0, 620, 620), new Vector2(0, 0));
                    break;

                case TurretButton.TurretType.Missile:
                    normalTex = game.Content.Load<Texture2D>(@"Texture/rocket_l");
                    range = new CenteredSprite(new Vector2(0, 0), game.Content.Load<Texture2D>(@"Texture\c-400-400"), new Rectangle(0, 0, 420, 420), new Vector2(0, 0));
                    break;

                case TurretButton.TurretType.Aura:
                    normalTex = game.Content.Load<Texture2D>(@"Texture/aura");
                    range = new CenteredSprite(new Vector2(0, 0), game.Content.Load<Texture2D>(@"Texture\c-200-200"), new Rectangle(0, 0, 220, 220), new Vector2(0, 0));
                    break;
            }

            baseTex = game.Content.Load<Texture2D>(@"Texture/turet_podnozje");
            selected = new Sprite(new Vector2(30.0f, 30.0f), normalTex, new Rectangle(0, 0, 40,40), Vector2.Zero);
        }

        public TurretButton.TurretType Type { get; set; }

        public void Update(MouseState mouseState, Game1 game)
        {
            Vector2 mouseLocation = new Vector2(mouseState.X, mouseState.Y);
            int price = 0;

            if (selected != null)
            {
                game.IsMouseVisible = false;

                selected.Location = mouseLocation + new Vector2(-20, -20);
                overlayRed.Location = mouseLocation + new Vector2(-20, -20);
                overlayGreen.Location = mouseLocation + new Vector2(-20, -20);
                range.Location = mouseLocation - new Vector2(range.Destination.Height / 2, range.Destination.Width / 2);

                //Check if possible to place turret
                Vector2 center = new Vector2(game.GraphicsDevice.DisplayMode.Width / 2, game.GraphicsDevice.DisplayMode.Height / 2);
                if ((Vector2.Distance(mouseLocation, center) > 101  && Vector2.Distance(mouseLocation, center) < 137)
                    || (Vector2.Distance(mouseLocation, center) > 222 && Vector2.Distance(mouseLocation, center) < 262)
                    || (Vector2.Distance(mouseLocation, center) > 345 && Vector2.Distance(mouseLocation, center) < 387))
                    canBuild = true;
                else
                    canBuild = false;


                switch (Type)
                {
                    case TurretButton.TurretType.Laser:
                        if (game.mothership.energy < 2400)
                        {
                            canBuild = false;
                            return;
                        }
                        else price = 2400;
                        break;
                    case TurretButton.TurretType.Catlling:
                        if (game.mothership.energy < 1600)
                        {
                            canBuild = false;
                            return;
                        }
                        else price = 800;
                        break;
                    case TurretButton.TurretType.Missile:
                        if (game.mothership.energy < 4000)
                        {
                            canBuild = false;
                            return;
                        }
                        else price = 4000;
                        break;
                    case TurretButton.TurretType.Aura:
                        if (game.mothership.energy < 5000)
                        {
                            canBuild = false;
                            return;
                        }
                        else price = 5000;
                        break;
                }
                foreach (Door door in game.mothership.DoorsList)
                {
                    if (Vector2.Distance(door.Position, mouseLocation) < 60)
                        canBuild = false;
                }
                /*
                foreach (Door door in game.mothership.DoorsList)
                {
                    if (Vector2.Distance(door.Position, mouseLocation) < 50)
                        selected.Texture = redTex;
                }*/

                foreach (Turret turret in game.mothership.TurretList)
                {
                    if (Vector2.Distance(turret.Position, mouseLocation) < 50)
                        canBuild = false;
                }
            }


            if (selected != null && mouseState.LeftButton == ButtonState.Pressed && (canBuild == true))
            {
                Sprite baseSprite = null;
                Sprite weaponSprite = null;



                baseSprite = new CenteredSprite(mouseLocation, baseTex, new Rectangle(0, 0, 40, 40), new Vector2(0, 0));

                switch (Type)
                {
                    case TurretButton.TurretType.Laser:
                         weaponSprite = new CenteredSprite(mouseLocation, normalTex, new Rectangle(0, 0, 40, 40), new Vector2(0, 0));
                        new LaserTurret(game.mothership, baseSprite, weaponSprite);
                        break;

                    case TurretButton.TurretType.Catlling:
                       weaponSprite = new CenteredSprite(mouseLocation, normalTex, new Rectangle(0, 0, 40, 40), new Vector2(0, 0));
                       CenteredSprite fireSprite = new CenteredSprite(mouseLocation,fire, new Rectangle(0, 0, 50, 50), Vector2.Zero);
                       fireSprite.AddFrame(new Rectangle(50, 0, 50, 50));
                       fireSprite.FrameTime = 0.03f;
                        new GattlingTurret(game.mothership, baseSprite, weaponSprite, fireSprite);
                        break;

                    case TurretButton.TurretType.Missile:
                       weaponSprite = new CenteredSprite(mouseLocation, normalTex, new Rectangle(0, 0, 40, 40), new Vector2(0, 0));
                        new MissileTurret(game.mothership, baseSprite, weaponSprite);
                        break;

                    case TurretButton.TurretType.Aura:
                        weaponSprite = new CenteredSprite(mouseLocation, normalTex, new Rectangle(0, 0, 40, 40), new Vector2(0, 0));
                        new AuraTurret(game.mothership, baseSprite, weaponSprite);
                        break;
                }
                game.mothership.removeEnergy(price);
                //   selected = null;
            }

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            selected.Draw(spriteBatch);
            if (canBuild == true)
            {
                range.Draw(spriteBatch);
                overlayGreen.Draw(spriteBatch);
            }
            else
                overlayRed.Draw(spriteBatch);
        }
    }
}
