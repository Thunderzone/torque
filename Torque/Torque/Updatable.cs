﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace LastHope
{
    interface Updatable
    {
        void Update(GameTime time);
    }
}
