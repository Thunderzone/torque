﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    class Button
    {
        public enum ButtonState
        {
            Normal, MouseOver, Selected
        }

        private Info info;

        public Info Info
        {
            set { info = value; }
        }

        ButtonState state;

        private bool visible = true;

        private Sprite normal;
        private Sprite mouseover;
        private Sprite selected;

        protected Sprite current;

        private Rectangle rectangle;

        public Button(Rectangle rectangle, Texture2D texture)
        {
            this.rectangle = rectangle;
            this.normal = new Sprite(new Vector2(rectangle.X, rectangle.Y), texture, new Rectangle(0, 0, rectangle.Width, rectangle.Height), Vector2.Zero);
            this.selected = new Sprite(new Vector2(rectangle.X, rectangle.Y), texture, new Rectangle(rectangle.Width, 0, rectangle.Width, rectangle.Height), Vector2.Zero);
            this.mouseover = new Sprite(new Vector2(rectangle.X, rectangle.Y), texture, new Rectangle(rectangle.Width * 2, 0, rectangle.Width, rectangle.Height), Vector2.Zero);

            this.current = normal;
        }

        public Rectangle Rectangle
        {
            get { return rectangle; }
        }

        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }


        public virtual bool IsOver(Point point)
        {
            return rectangle.Intersects(new Rectangle(point.X, point.Y, 1, 1));
        }

        public ButtonState State
        {
            get { return state; }
            set
            {
                state = value;
                switch (state)
                {
                    case ButtonState.Normal:
                        current = normal;
                        break;
                    case ButtonState.Selected:
                        current = selected;
                        break;
                    case ButtonState.MouseOver:
                        current = mouseover;
                        break;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (visible)
            {
                current.Draw(spriteBatch);

            }
        }

    }
}
