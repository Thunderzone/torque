﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace LastHope
{
    public class TextLabel : Drawable
    {
        public String text { get; set; }
        private SpriteFont font;
        public Vector2 position { get; set; }
        public Color color { get; set; }

        public TextLabel(String label, Vector2 position, Color color, SpriteFont spriteFont, TextOverlay parent)
        {
            text = label;
            this.position = position;
            this.color = color;

            this.font = spriteFont;
            parent.addText(this);
        }

        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, text, position, color);
        }
    }
}
