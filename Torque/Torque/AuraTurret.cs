﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastHope
{
    class AuraTurret : Turret
    {
        public const float AOE = 100.0f;
        public AuraTurret(Mothership parent, Sprite spriteBase, Sprite spriteGun)
            : base(0, 0, 0, (int) AOE, 0f, parent, spriteBase, spriteGun)
        {
            Texture2D tex = parent.parent.Content.Load<Texture2D>(@"Texture\c-200-200");
            //RangeSprite = new CenteredSprite(spriteBase.Center, tex, new Rectangle(0, 0, range, range), new Vector2(0, 0));
            RangeSprite = new CenteredSprite(spriteBase.Center, tex, new Rectangle(0, 0, 220, 220), new Vector2(0, 0));
        }

        public override Enemy getTarget() { return null; }

        public override void shoot(Enemy e, int dmg, double rotation)
        {
            throw new NotImplementedException();
        }
    }
}
