﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastHope
{
    class GattlingTurret : Turret
    {

        private Sprite fire;

        public GattlingTurret( Mothership parent, Sprite spriteBase, Sprite spriteGun, Sprite fire)
            : base( 10, 30, 120, 300, 0.3f, parent, spriteBase, spriteGun )
        {
            this.fire = fire;
            sprites.Add( fire );
            Texture2D tex = parent.parent.Content.Load<Texture2D>(@"Texture\c-600-600");
            //RangeSprite = new CenteredSprite(spriteBase.Center, tex, new Rectangle(0, 0, range, range), new Vector2(0, 0));
            RangeSprite = new CenteredSprite(spriteBase.Center, tex, new Rectangle(0, 0, 620, 620), new Vector2(0, 0));
        }

        public override void shoot(Enemy enemy, int dmg, double rotation)
        {
            Vector2 addition = new Vector2(-(float)Math.Sin(rotation) * 20, (float)Math.Cos(rotation) * 20);
            Texture2D spriteImage = parent.parent.Content.Load<Texture2D>(@"Texture/plasma"); // depends
            Sprite sprite = new CenteredSprite(spriteBase.Center + addition, spriteImage, new Rectangle(0, 0, 10, 10), new Vector2(0, 0));

            // instant damage! need animation
            enemy.dealDamage(dmg);
            SoundManager.PlayGatlingShot();
        }

        protected override void SetRotation()
        {
            fire.Rotation = spriteGun.Rotation;
        }
    }
}
