﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastHope
{
    // kasnije cemo ovo dole
    enum ProjectileType { LASER, MISSILE };

    public class Projectile : Drawable, Updatable
    {
        protected int damage;
        // dodati AoE projektil
        protected Enemy target;
        protected Sprite sprite;
        protected ProjectileMgr projectileMgr;
        protected float speed = 350.0f;

        public Projectile( int _damage, Enemy _target, Sprite _sprite, float _speed, ProjectileMgr parent )
        {
            damage = _damage;
            target = _target;
            sprite = _sprite;
            speed = _speed;
            projectileMgr = parent;

            projectileMgr.addProjectile(this);
        }

        protected virtual bool colision()
        {
            return Vector2.Distance(sprite.Center, target.Position) < 30;
        }

        void Updatable.Update(GameTime time)
        {
            sprite.Update(time);

            if (colision()) onHit();

            // update velocity
            calcVelocity();
        }

        protected virtual void calcVelocity()
        {
            //Calculate new velocity vector
            Vector2 targetLocation = target.Position;
            Vector2 newVelocity = targetLocation - sprite.Center;
            newVelocity.Normalize();
            newVelocity *= speed;

            //Update velocity vector
            sprite.Velocity = newVelocity;
        }

        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);
        }

        protected virtual void onHit()
        {
            // deal damage, but can do other things too
            target.dealDamage(damage);

            // delete me please
            projectileMgr.destroyProjectile(this);
        }
    }
}
