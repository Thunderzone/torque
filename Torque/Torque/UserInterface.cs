﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    class UserInterface : Drawable
    {
        private List<Button> buttons = new List<Button>();

        private TurretPalette turretPalette;
        private TurretBuilder turretBuilder;
        Game1 game;

      
        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            Drawable drawable = turretPalette;
            drawable.Draw(spriteBatch);

            if(turretBuilder != null)
               turretBuilder.Draw(spriteBatch);
            /*
            foreach (Button button in buttons)
            {
                button.Draw(spriteBatch);
            }
            /*
            towerBtn.Draw(spriteBatch);
            
            if (selected != null)
                selected.Draw(spriteBatch);
            */
        }

        public void AddButton(Button button)
        {
            buttons.Add(button);
        }

        public void Update(MouseState mouseState)
        {
            turretPalette.Update(mouseState);

            if (turretPalette.IsDropState)
            {
                if (turretBuilder == null)
                    turretBuilder = new TurretBuilder(game, turretPalette.TurrentType);

                if (turretPalette.TurrentType != turretBuilder.Type)
                {
                    turretBuilder.Type = turretPalette.TurrentType;
                }

               turretBuilder.Update(mouseState, game);
            }
            else
            {
                turretBuilder = null;
            }
        }

        public UserInterface(Game1 game)
        {
            turretPalette = new TurretPalette(game);
            this.game = game;
        }

    }
}