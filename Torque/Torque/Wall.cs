﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastHope
{
    public class Wall : Updatable, Drawable
    {
        void Updatable.Update(GameTime time)
        {
            Sprite.Update(time);
        }

        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            Sprite.Draw(spriteBatch);
        }


        public Vector2 Position 
        {
             set 
            {
                Sprite.Location = value - new Vector2(Sprite.Destination.Height / 2, Sprite.Destination.Width / 2);
            }

            get
            {
                return Sprite.Center; 
            }
        }
        public Vector2 Orientation
        {
            private set { }
            get
            {
                return Polar.AngleToVec(Sprite.Rotation);
            }
        }
        public Sprite Sprite;


        public void OrientateTo(Vector2 pos)
        {
            Sprite.Rotation = (float)Polar.VecToAngle((pos - Sprite.Center)); // da li nam ovo treba
        }

        public Wall(Vector2 pos, Mothership parent)
        {
            Vector2 position = pos + parent.Sprite.Location;
            Texture2D spriteImage = parent.parent.Content.Load<Texture2D>(@"Texture/energy-wall");
            Sprite = new CenteredSprite(position, spriteImage, new Rectangle(0, 0, 45, 45), new Vector2(0, 0));
            Sprite.AddFrame(new Rectangle(45,0,45,45));
            Sprite.FrameTime = 0.05F;
            OrientateTo(parent.Sprite.Center);
        }
    }
}
