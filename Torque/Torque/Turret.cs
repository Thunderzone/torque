﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LastHope
{
    public abstract class Turret : Drawable, Updatable
    {
        protected int minDmg, maxDmg;
        protected int firingRate; // miliseconds per shot
        protected int range;
        protected float rotationSpeed;
        protected List<Sprite> sprites = new List<Sprite>();

        protected Mothership parent;
        public Vector2 Position { get { return spriteBase.Center; } }

        protected Sprite spriteBase, spriteGun;  

        private float timeElapsedSinceShot = 0.0f;
        private Enemy lockedEnemy = null;
       // private bool finishedRotating = false;

        bool isShot = false;

        private Random rand = new Random();

        public Sprite RangeSprite;
        public bool RangeVisible = false;

        public Turret(int minDmg, int maxDmg, int firingRate, int range, float rotationSpeed, Mothership parent, Sprite spriteBase, Sprite spriteGun)
        {
            this.minDmg = minDmg;
            this.maxDmg = maxDmg;
            this.firingRate = firingRate;
            this.range = range;
            this.rotationSpeed = rotationSpeed;
            this.parent = parent;
            this.spriteBase = spriteBase;
            this.spriteGun = spriteGun;

            //sprites.Add(spriteBase);
            //sprites.Add(spriteGun);
            parent.addTurret(this);

            Texture2D tex = parent.parent.Content.Load<Texture2D>(@"Texture\range-circle");
            //RangeSprite = new CenteredSprite(spriteBase.Center, tex, new Rectangle(0, 0, range, range), new Vector2(0, 0));
            RangeSprite = new CenteredSprite(spriteBase.Center, tex, new Rectangle(0, 0, 300, 300), new Vector2(0, 0));
           // RangeVisible = true;
        }



        protected virtual void SetRotation()
        {
        }

        void Updatable.Update(GameTime time)
        {

            if (Keyboard.GetState().IsKeyDown(Keys.LeftControl) || Keyboard.GetState().IsKeyDown(Keys.RightControl))
            {
                RangeVisible = true;
            }
            else
                RangeVisible = false;

            if (null != lockedEnemy)
            {               

                if (!checkRange() || 0.0f == lockedEnemy.hitpoints) // if out of range or dead
                {
                    lockedEnemy = null;
                    timeElapsedSinceShot = 0.0f;
                    return;
                }
                
                //If locked on enemy
                float targetOrientation = (float)Polar.VecToAngle(lockedEnemy.Position - spriteGun.Center);
                float omega = targetOrientation - spriteGun.Rotation;
                if (Math.Abs(omega)  > 0.05f)
                {
                    spriteGun.Rotation += Math.Sign(omega) * rotationSpeed * MathHelper.TwoPi/360 * time.ElapsedGameTime.Milliseconds  ;
                }

                timeElapsedSinceShot += time.ElapsedGameTime.Milliseconds;
                
                // check for an aura tower nearby
                int currentFiringRate = firingRate;

                int auraCnt = parent.getAurasInRange(spriteBase.Center, AuraTurret.AOE );
                currentFiringRate -= auraCnt * (int) Math.Round( 0.2 * firingRate );

                if (currentFiringRate <= timeElapsedSinceShot && Math.Abs(omega) < 0.25f)
                {
                    // BOOOM! Headshot

                    isShot = true;

                    shoot(lockedEnemy, rand.Next(minDmg, maxDmg), spriteGun.Rotation);
                    timeElapsedSinceShot = 0.0f;
                }
                else isShot = false;

                foreach (Sprite s in sprites)
                {
                    s.Update(time);
                }
            }
            else 
                // nothing locked, get a target
                lockedEnemy = getTarget();

            SetRotation();
        }

        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            foreach (Sprite s in sprites)
                if(isShot)
                    s.Draw(spriteBatch);

            spriteBase.Draw(spriteBatch);
            spriteGun.Draw(spriteBatch);

            if (RangeVisible)
                RangeSprite.Draw(spriteBatch);
        }

        public bool checkRange()
        {
            if (Vector2.Distance(lockedEnemy.Position, spriteBase.Center) <= range)
                return true;

            // try to acquire new target
            lockedEnemy = getTarget();
            
            return lockedEnemy != null;
        }

        public abstract void shoot( Enemy e, int dmg, double rotation );

        public virtual Enemy getTarget()
        {
            // we can implement some other targeting strategies here
            Enemy target = parent.waveManager.getNearest(spriteBase.Center);
            if( null == target ) return null;

            if (Vector2.Distance(target.Position, spriteBase.Center) > range)
                return null;

            return target;
        }
    }
}
