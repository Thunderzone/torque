﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    class Core
    {
        private CenteredSprite sprite;

        private float scale = 1.0f;

        private Game1 game;

        public Core(Game1 game)
        {
            this.game = game;

            Texture2D texture = game.Content.Load<Texture2D>(@"Texture/core");

            sprite = new CenteredSprite(new Vector2(game.GraphicsDevice.DisplayMode.Width / 2, game.GraphicsDevice.DisplayMode.Height / 2) , texture, new Rectangle(0, 0, 200, 200), Vector2.Zero);


        }

        public void Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);
        }

        public void Update(GameTime time)
        {
            sprite.Rotation += MathHelper.Pi / 360 * time.ElapsedGameTime.Milliseconds / 200;

            sprite.Update(time);
        }
    }
}
