﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace LastHope
{
    public class TextOverlay : Drawable
    {
        public SpriteFont pericles36;
        public SpriteFont timerFont;
        public SpriteFont sf;
        List<TextLabel> labelList = new List<TextLabel>();

        public TextOverlay(Game1 game)
        {
            pericles36 = game.Content.Load<SpriteFont>(@"Fonts/Pericles36");
            timerFont = game.Content.Load<SpriteFont>(@"Fonts/Timer");
            sf = game.Content.Load<SpriteFont>(@"Fonts/Pericles35");
        }

        public void addText(TextLabel text)
        {
            labelList.Add(text);
        }

        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            foreach (TextLabel t in labelList)
            {
               ((Drawable)t).Draw(spriteBatch);
            }
        }

        public void removeText(TextLabel text)
        {
            labelList.Remove(text);
            //labelList.Take< TextLabel >(
        }
    }
}
