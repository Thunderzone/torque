﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace LastHope
{
    class Path
    {
        public Waypoint GetNextWaypoint()
        {
            if (Waypoints.Count == 0)
                return null;
            else
            {
                Waypoint ret = Waypoints[0];
                Waypoints.RemoveAt(0);
                return ret;
            }
        }

        public Waypoint PeekNextWaypoint()
        {
            if (Waypoints.Count == 0)
                return null;
            else
            {
                return Waypoints[0];
            }
        }

        public Waypoint GetLastWaypoint()
        {
            if (Waypoints.Count == 0)
                return null;
            return Waypoints.Last<Waypoint>();
        }

        public int Count() { return Waypoints.Count; }

        public void AddWaypoint(Waypoint waypoint)
        {
            Waypoints.Add(waypoint);
        }

        public void Clear()
        {
            Waypoints.Clear();
        }

        public Waypoint Last()
        {
            if (Waypoints.Count == 0)
                return null;
            else
                return Waypoints.Last();
        }

        public void RemoveFirst(int n)
        {
            Waypoints.RemoveRange(0, n);
        }

        private List<Waypoint> Waypoints = new List<Waypoint>();
    }
}
