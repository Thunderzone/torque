﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    public class WaveManager : Updatable, Drawable
    {
        List<Enemy> enemies = new List< Enemy >();
        List<Enemy> enemiesDeletion = new List<Enemy>();

        const int BASE_HITPOINTS = 50;
        const int BASE_ARMOR = 1;
        const int BASE_BOUNTY = 20;
        const int BASE_NUMBER = 15;
        const int BASE_TIMER = 5000;
        const int BASE_SPEED = 50;

        bool pause = true;
        int pauseTimer;

        private Random rand = new Random();

        public int level { get; private set; }
       
        const int creepTimerMax = 1000, creepTimerMin = 200;
        const float WAVE_PROGRESSION_COEFF = 1.1f;

        float timeElapsedSincePause = 0.0f;
        float timeElapsedSinceLastSpawn = 0.0f;
        float timeElapsedSinceWaveBegun = 0.0f;
        float timeElapsedSinceTimerUpdate = 0.0f;

        int toBeSpawned;
        int whenToSpawn;

        public Mothership parent
        { get; private set;  }

        public WaveManager(Mothership parent)
        {
            this.parent = parent;
            this.level = 1;

            startSpawnWave();
        }

        public void startSpawnWave()
        {
            // number of creeps to be spawned
            toBeSpawned = rand.Next( 30, 50 );

            // when is the next wave
            pauseTimer = 10000;
            pause = true;

            // spawn a creep instantly at the end of pause
            whenToSpawn = 0;
            timeElapsedSinceLastSpawn = 0.0f;
            // KOLIKO IMA SEKUNDI DO TALASAAA!!!
        }
        
        void Drawable.Draw(SpriteBatch spriteBatch)
        {
           foreach( Enemy e in enemies )
               ((Drawable)e).Draw( spriteBatch );
        }

        private bool first_spawn = true;

        void Updatable.Update(GameTime time)
        {
            timeElapsedSinceTimerUpdate += time.ElapsedGameTime.Milliseconds;

            foreach (Enemy e in enemies)
            {
                ((Updatable)e).Update(time);
            }

            foreach (Enemy e in enemiesDeletion)
                enemies.Remove(e);

            enemiesDeletion.Clear();

            if (pause)
            {
                timeElapsedSincePause += time.ElapsedGameTime.Milliseconds;

                if (first_spawn)
                    if (timeElapsedSincePause >= 7000)
                    {
                        SoundManager.PlayCntdown();
                        first_spawn = false;
                    }

                if (pauseTimer <= timeElapsedSincePause)
                {
                    pause = false;
                    timeElapsedSincePause = 0;
                }

                if (timeElapsedSinceTimerUpdate >= 1000)
                {
                    parent.setLevel( level, true);
                    timeElapsedSinceTimerUpdate = 0.0f;
                }

                return;
            }

            timeElapsedSinceWaveBegun += time.ElapsedGameTime.Milliseconds;
            timeElapsedSinceLastSpawn += time.ElapsedGameTime.Milliseconds;
            if( whenToSpawn <= timeElapsedSinceLastSpawn )
            {
                // we're spawning, baby!
                // random wave composition?
                int ran = rand.Next(0, 4);
                Texture2D spriteImage = null;

                switch (ran)
                {
                    case 0:
                        spriteImage = parent.parent.Content.Load<Texture2D>(@"Texture/ship");
                        break;
                    case 1:
                        spriteImage = parent.parent.Content.Load<Texture2D>(@"Texture/enemy1");
                        break;
                    case 2:
                        spriteImage = parent.parent.Content.Load<Texture2D>(@"Texture/enemy2");
                        break;
                    case 3:
                        spriteImage = parent.parent.Content.Load<Texture2D>(@"Texture/enemy3");
                        break;

                }

                //Texture2D spriteImage = parent.parent.Content.Load<Texture2D>(@"Texture/ship");

                // stats
                int hitpoints = (int)Math.Round((level + 1) * BASE_HITPOINTS * WAVE_PROGRESSION_COEFF);
                int armor = (int)Math.Round((level + 1) * BASE_ARMOR * WAVE_PROGRESSION_COEFF);

                int bounty = (int)Math.Round(WAVE_PROGRESSION_COEFF * (level + 1) * BASE_BOUNTY);
                int speed = rand.Next( 2 * BASE_SPEED - 15, 2 * BASE_SPEED + 15);

                // random spawn
                Vector2 pos = new Vector2();
                pos.X = rand.Next(parent.parent.GraphicsDevice.DisplayMode.Width + 50, parent.parent.GraphicsDevice.DisplayMode.Width + 100);
                pos.Y = rand.Next(parent.parent.GraphicsDevice.DisplayMode.Height + 50, parent.parent.GraphicsDevice.DisplayMode.Height + 100);

                // they all point towards the center
                Enemy new_enemy = new Enemy(hitpoints, armor, bounty, speed, this, pos, new Vector2(1024/2, 768/2), spriteImage);
                enemies.Add(new_enemy);
                new_enemy.RecalculatePath(parent);

                --toBeSpawned;
                if (0 == toBeSpawned)
                {
                    startSpawnWave();
                    ++level;
                }
                else
                {
                    // time to the next spawn
                    whenToSpawn = rand.Next(creepTimerMin, creepTimerMax);
                    timeElapsedSinceLastSpawn = 0.0f;
                }       
                
            }
            if (timeElapsedSinceTimerUpdate >= 1000)
            {
                parent.setLevel( level, false);

                timeElapsedSinceTimerUpdate = 0.0f;
            }
        }

        public void kill(Enemy target)
        {
            enemiesDeletion.Add(target);
            parent.explosionManager.AddExplosion(target.sprite.Center, new Vector2(0, 0));
        }

        public Enemy getNearest(Vector2 pos)
        {
            if (0 == enemies.Count) return null;

            float minDist = 10000f;
            Enemy ret = null;

            foreach (Enemy e in enemies)
            {
                float currDist = Vector2.Distance(e.Position, pos);
                if (currDist < minDist)
                {
                    minDist = currDist;
                    ret = e;
                }
            }

            return ret;
        }
        public List<Enemy> getEnemiesInRange(Vector2 pos, float range)
        {
            List<Enemy> ret = new List< Enemy >();
            foreach (Enemy e in enemies)
            {
                if (Vector2.Distance(e.Position, pos) <= range)
                    ret.Add(e);
            }

            return ret;
        }

        public void RecalculatePaths()
        {

            foreach (Enemy e in enemies)
            {
                e.RecalculatePath(parent);
            }
        }
    }
}
