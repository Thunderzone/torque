﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    public class Mothership : Updatable, Drawable
    {
        private Core core;
        public List<Door> DoorsList { get; private set; }
        public List<Wall> ActiveWallsList { get; private set; } // aktivni zidovi samo !!!
        public List<Turret> TurretList { get; private set; }

        public ProjectileMgr ProjectileMgr { get; private set; }

        public const float RING_WIDTH = 60.0F;

        public Vector2 Position { get { return Sprite.Center; } }
        public Sprite Sprite;
        public Game1 parent
        { get; private set; }

        int life = 100;
        public int energy { get; set; }
        int level = 1;

        public float powerup = 10;
        public Sprite PowerupSprite;

        public TextLabel lifeLabel { get; set; }
        public TextLabel energyLabel;
        public TextLabel levelLabel { get; set; }

        private Sprite energySprite;
        private Sprite coreLifeSprite;
        
        public void decrementLife() { 
            --life;
            lifeLabel.text = life.ToString();
            if (0 == life) gameOver();
        }
        public void addEnergy(int ammount)
        {
            energy += ammount;
            energyLabel.text = energy.ToString();
        }
        public void removeEnergy(int ammount)
        {
            energy -= ammount;
            energyLabel.text = energy.ToString();
        }

        public void setLevel(int lvl, bool pause)
        {
            if (pause) levelLabel.color = Color.Yellow;
            else levelLabel.color = Color.White;
            level = lvl;
            levelLabel.text = level.ToString();
        }

        public void gameOver()
        {
            SoundManager.PlayGameover();
            parent.currentGameState = Game1.GameStates.GameOver;
        }

        public ExplosionManager explosionManager { get; private set; }
        public int getAurasInRange(Vector2 pos, float range)
        {
            int ret = 0;
            foreach (Turret t in TurretList)
            {
                if (t is AuraTurret) ++ret;
            }
            return ret;
        }

        public Mothership(Game1 parent)
        {
            this.parent = parent;
            this.energy = 2000;
          this.life = 100;
            waveManager = new WaveManager(this);
            ProjectileMgr = new ProjectileMgr(this);

            core = new Core(parent);

            //Create sprite
            Texture2D spriteImage = parent.Content.Load<Texture2D>(@"Texture/station");
            Vector2 location = new Vector2(parent.GraphicsDevice.DisplayMode.Width / 2, parent.GraphicsDevice.DisplayMode.Height / 2);
            Sprite = new CenteredSprite(location, spriteImage, new Rectangle(0, 0, 800, 800), new Vector2(0, 0));

            //Create explosion manager
            Texture2D explosionTxt = parent.Content.Load<Texture2D>(@"Texture/explosion");
            explosionManager = new ExplosionManager(explosionTxt, new Rectangle(0, 0, 50, 50), 3, new Rectangle(0, 50, 50, 50));

            // dodavanje vrata i zidova
            DoorsList = new List<Door>();
            ActiveWallsList = new List<Wall>();
            TurretList = new List<Turret>();
            DoorsList.Add(new Door(new Vector2(628,693),this));
            DoorsList.Add(new Door(new Vector2(388, 645), this));
            DoorsList.Add(new Door(new Vector2(279, 389), this));

            ActiveWallsList.Add(new Wall(new Vector2(119, 526), this));
            ActiveWallsList.Add(new Wall(new Vector2(231, 475), this));

            // Add life label
            Texture2D energyTex = parent.Content.Load<Texture2D>(@"Texture/energy-resource");
            energySprite = new Sprite(new Vector2(parent.GraphicsDevice.DisplayMode.Width / 2 - 130, 20.0f), energyTex, new Rectangle(0, 0, 40, 40), Vector2.Zero);
            energyLabel = new TextLabel(energy.ToString(), new Vector2(parent.GraphicsDevice.DisplayMode.Width / 2 - 130 + 40, 20.0f), Color.White, parent.textOverlay.pericles36, parent.textOverlay);

            Texture2D coreLifeTex = parent.Content.Load<Texture2D>(@"Texture/core-life");
            coreLifeSprite = new Sprite(new Vector2(parent.GraphicsDevice.DisplayMode.Width / 2 + 130, 20.0f), coreLifeTex, new Rectangle(0, 0, 40, 40), Vector2.Zero);
            lifeLabel = new TextLabel(life.ToString(), new Vector2(parent.GraphicsDevice.DisplayMode.Width / 2 + 130 + 40, 20.0f), Color.White, parent.textOverlay.pericles36, parent.textOverlay);

            levelLabel = new TextLabel(level.ToString(), new Vector2(93, 80), Color.Yellow, parent.textOverlay.timerFont, parent.textOverlay);
            // Add wave counter

            Texture2D tex = parent.Content.Load<Texture2D>(@"Texture\Health");
            PowerupSprite = new CenteredSprite( new Vector2(parent.GraphicsDevice.DisplayMode.Width / 2 + 300,35), tex, new Rectangle(0, 0, 3, 3), new Vector2(0, 0));

        //    SoundManager.PlayPrepare();

        }

        public void addTurret(Turret t)
        {
            TurretList.Add(t);
        }


        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(PowerupSprite.Texture, PowerupSprite.Location, new Rectangle(0, 0, 3, 3), Color.Lerp(Color.Blue, Color.White, powerup/10), 0, new Vector2(0, 0), new Vector2(powerup*10, 5), SpriteEffects.None, 0);
            Sprite.Draw(spriteBatch);
            ((Drawable)waveManager).Draw(spriteBatch);
            //foreach (Drawable door in DoorsList)
              //  door.Draw(spriteBatch);

            foreach (Drawable wall in ActiveWallsList)
                wall.Draw(spriteBatch);

            foreach (Turret t in TurretList)
                ((Drawable)t).Draw(spriteBatch);

            explosionManager.Draw(spriteBatch);

            ((Drawable)ProjectileMgr).Draw(spriteBatch);

            energySprite.Draw(spriteBatch);
            coreLifeSprite.Draw(spriteBatch);

            core.Draw(spriteBatch);
        }

        private int S = 0, D = 0;
        private bool mSpressed = false, mDpressed = false;

        public const float POWERUP_COST = 5.0F;
        void Updatable.Update(GameTime time)
        {
            powerup += time.ElapsedGameTime.Milliseconds / 100.0F * 1.0F / 10.0F;
            if (powerup >= 10.0F)
                powerup = 10.0F;
            else if (powerup <= 0.0F)
                powerup = 0.0F;

            core.Update(time);

            ((Updatable)waveManager).Update( time );
            foreach (Updatable door in DoorsList)
                door.Update(time);
            foreach (Turret t in TurretList)
                ((Updatable)t).Update(time);

            explosionManager.Update(time);
            foreach (Updatable wall in ActiveWallsList)
                wall.Update(time);

            ((Updatable)ProjectileMgr).Update(time);

            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                mSpressed = true;
            }

            if (Keyboard.GetState().IsKeyUp(Keys.S))
            {
                if (mSpressed && powerup > POWERUP_COST)
                {
                    S++;
                    Vector2 next = new Vector2(119, 526);
                    switch (S % 4)
                    {
                        case 0:
                            next = new Vector2(119, 526);
                            break;
                        case 1:
                            next = new Vector2(520, 682);
                            break;
                        case 2:
                            next = new Vector2(681, 267);
                            break;
                        case 3:
                            next = new Vector2(258, 124);
                            break;
                    }

                    ActiveWallsList[0].Position = this.Sprite.Location + next;
                    waveManager.RecalculatePaths();
                    ActiveWallsList[0].OrientateTo(Sprite.Center);
                    powerup -= POWERUP_COST;
                }
                mSpressed = false;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                mDpressed = true;
            }

            if (Keyboard.GetState().IsKeyUp(Keys.D))
            {
                if (mDpressed && powerup > POWERUP_COST)
                {
                    D++;
                    Vector2 next = new Vector2(179, 360);
                    switch (D % 4)
                    {
                        case 0:
                            next = new Vector2(231, 475);
                            break;
                        case 1:
                            next = new Vector2(469, 569);
                            break;
                        case 2:
                            next = new Vector2(566, 320);
                            break;
                        case 3:
                            next = new Vector2(312, 234);
                            break;
                    }

                    ActiveWallsList[1].Position = this.Sprite.Location + next;
                    waveManager.RecalculatePaths();
                    ActiveWallsList[1].OrientateTo(Sprite.Center);
                    powerup -= POWERUP_COST;
                }
                mDpressed = false;
            }
        }

        public WaveManager waveManager { get; private set; }
    }

}
