﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastHope
{
    class MissileTurret : Turret
    {
        public MissileTurret( Mothership parent, Sprite spriteBase, Sprite spriteGun)
            : base( 200, 300, 2000, 200, 0.08f, parent, spriteBase, spriteGun )
        {
            Texture2D tex = parent.parent.Content.Load<Texture2D>(@"Texture\c-400-400");
            //RangeSprite = new CenteredSprite(spriteBase.Center, tex, new Rectangle(0, 0, range, range), new Vector2(0, 0));
            RangeSprite = new CenteredSprite(spriteBase.Center, tex, new Rectangle(0, 0, 420, 420), new Vector2(0, 0));
        }

        public override void shoot(Enemy enemy, int dmg, double rotation)
        {
            Vector2 addition = new Vector2(-(float)Math.Sin(rotation) * 20, (float)Math.Cos(rotation) * 20);
            Texture2D spriteImage = parent.parent.Content.Load<Texture2D>(@"Texture/plasma"); // depends
            Sprite sprite = new CenteredSprite(spriteBase.Center + addition, spriteImage, new Rectangle(0, 0, 10, 10), new Vector2(0, 0));

            Missile m = new Missile(dmg, enemy, sprite, parent.ProjectileMgr);

            SoundManager.PlayMissileLaunch();
        }
    }
}
