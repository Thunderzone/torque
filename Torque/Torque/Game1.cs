using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        UserInterface userInterface;
        DeepSpace deepSpace;
        public TextOverlay textOverlay { get; private set; }


        public Mothership mothership { get; private set; }

        enum States { Nothing, Building };
        States currentState;
        public StatesManager stateManager;

        public enum GameStates { StartScreen, Credits, Highscore, Playing, Quit, GameOver, Instructions };
        public GameStates currentGameState { get; set; }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            DisplayMode dm = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode;

            graphics.IsFullScreen = true;

            //if (graphics.IsFullScreen)
            //{
            //    graphics.PreferredBackBufferFormat = dm.Format;
            //    graphics.PreferredBackBufferHeight = dm.Height;
            //    graphics.PreferredBackBufferWidth = dm.Width;
            ////}
            //else
            //{
            graphics.PreferredBackBufferHeight = 1024;
            graphics.PreferredBackBufferWidth = 1280;
            //}


            currentGameState = GameStates.StartScreen;


            currentState = States.Nothing;
            //textOverlay = new TextOverlay(this);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            System.Windows.Forms.Cursor.Show();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            SoundManager.Initialize(Content);
            // TODO: use this.Content to load your game content here
            textOverlay = new TextOverlay(this);
            userInterface = new UserInterface(this);
            deepSpace = new DeepSpace(this);

            mothership = new Mothership(this);

            //
            stateManager = new StatesManager(this);


        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        public bool first = true;

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            MouseState mouseState = Mouse.GetState();
            KeyboardState keyboardState = Keyboard.GetState();




            switch (currentGameState)
            {
                case GameStates.Playing:
                    if (first)
                    {
                        SoundManager.PlayPrepare();
                        first = false;
                    }

                    userInterface.Update(mouseState);

                    Updatable ideepSpace = deepSpace;
                    ideepSpace.Update(gameTime);

                    ((Updatable)mothership).Update(gameTime);

                    if (keyboardState.IsKeyDown(Keys.Escape))
                    {
                        first = true;
                        currentGameState = GameStates.StartScreen;
                    }

                    break;

                case GameStates.StartScreen:
                    stateManager.Update();

                    //if (keyboardState.IsKeyDown(Keys.Escape))
                    //{
                    //    this.Exit();
                    //}

                    break;

                case GameStates.Credits:
                    stateManager.Update();
                    break;

                case GameStates.GameOver:
                    SoundManager.Initialize(Content);
                    // TODO: use this.Content to load your game content here
                    textOverlay = new TextOverlay(this);
                    userInterface = new UserInterface(this);
                    deepSpace = new DeepSpace(this);

                    mothership = new Mothership(this);

                    //
                    stateManager = new StatesManager(this);
                    stateManager.Update();

                    break;

                case GameStates.Instructions:
                    stateManager.Update();
                    break;

                case GameStates.Quit:
                    this.Exit();
                    break;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            switch (currentGameState)
            {
                case GameStates.Playing:
                    Drawable ideepSpace = deepSpace;
                    ideepSpace.Draw(spriteBatch);

                    ((Drawable)mothership).Draw(spriteBatch);
                    ((Drawable)userInterface).Draw(spriteBatch);
                    ((Drawable)textOverlay).Draw(spriteBatch);
                    break;


                case GameStates.StartScreen:
                    stateManager.Draw(spriteBatch);
                    break;

                case GameStates.Credits:
                    stateManager.Draw(spriteBatch);
                    break;


                case GameStates.Instructions:
                    stateManager.Draw(spriteBatch);
                    break;

                case GameStates.GameOver:
                    stateManager.Draw(spriteBatch);
                    break;
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
