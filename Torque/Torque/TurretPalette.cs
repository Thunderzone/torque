﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace LastHope
{
    class TurretPalette: Drawable
    {
        private Game game;
        private Sprite toolCenter;
        private float angle = - MathHelper.Pi / 6;

        TurretButton.TurretType turretType;

        TurretButton selectedTurret;
        bool isDropState = false;

        Vector2 centerPosition = new Vector2(50.0f, 50.0f);

        private List<TurretButton> turrets = new List<TurretButton>();
        private Info info;

        void Drawable.Draw(SpriteBatch spriteBatch)
        {
            
            foreach (TurretButton turret in turrets)
            {
                if (turret.Visible)
                {
                    turret.Draw(spriteBatch);
                }
            }
            
            toolCenter.Draw(spriteBatch);

            info.Draw(spriteBatch);

        }

        public void Update(MouseState mouseState)
        {
            bool isOverAny = false;

            foreach (TurretButton turret in turrets)
            {
                bool isOver = turret.IsOver(new Point(mouseState.X, mouseState.Y));
                bool isMouseClicked = mouseState.LeftButton == ButtonState.Pressed ? true : false;

                

                if (isOver)
                {
                    if (turret.State != Button.ButtonState.Selected)
                    {
                        turret.State = Button.ButtonState.MouseOver;

                        if (isMouseClicked)
                        {
                            turret.State = Button.ButtonState.Selected;
                            isDropState = true;
                            turretType = turret.Type;

                            if (selectedTurret != null) selectedTurret.State = Button.ButtonState.Normal;
                            selectedTurret = turret;
                        }
                    }
                    info.Show(turret.Type);
                    isOverAny = true;
                }
                else
                {
                    if (turret.State != Button.ButtonState.Selected)
                    {
                        turret.State = Button.ButtonState.Normal;

                    }
                    
                }
            }

            if (mouseState.RightButton == ButtonState.Pressed)
            {
                Deselect();
                selectedTurret = null;
                isDropState = false;
            }

            if (!isOverAny)
            {
                info.Hide();
            }
        }

        public TurretPalette(Game game)
        {
            this.game = game;

            info = new Info(game, new Vector2(200.0f, 30.0f));

            AddTurret(TurretButton.TurretType.Laser);
            AddTurret(TurretButton.TurretType.Catlling);
            AddTurret(TurretButton.TurretType.Aura);
            AddTurret(TurretButton.TurretType.Missile);

            Texture2D centerTex = game.Content.Load<Texture2D>(@"Texture/palette_centar-t");
            toolCenter = new Sprite(new Vector2(centerPosition.X, centerPosition.Y), centerTex, new Rectangle(0,0,100,100), Vector2.Zero);
        }

        public void Deselect()
        {
            if(selectedTurret != null)selectedTurret.State = Button.ButtonState.Normal;
        }

        public bool IsDropState
        {
            get { return isDropState; }
        }

        public TurretButton.TurretType TurrentType
        {
            get { return turretType; }
        }

        public void AddTurret(TurretButton.TurretType type)
        {
            float ro = 60.0f;

            Vector2 origin = new Vector2(-35.0f, -35.0f);
            Vector2 centerOrigin = centerPosition + new Vector2(50.0f, 50.0f);
            Vector2 position = centerOrigin + new Vector2(ro * (float)Math.Cos(angle), ro * (float)Math.Sin(angle)) + origin;
            Texture2D texture = game.Content.Load<Texture2D>(GetTexturePath(type));
            TurretButton turret = new TurretButton((Game1)game, new Rectangle((int)position.X, (int)position.Y, 70, 70), texture, 30.0f, type);
            turrets.Add(turret);
            
            angle += 2 * MathHelper.Pi / 6;
        }

        private String GetTexturePath(TurretButton.TurretType type)
        {
            switch(type)
            {
                case TurretButton.TurretType.Catlling:
                    return @"Texture/turret-sattling-button";
                case TurretButton.TurretType.Laser:
                    return @"Texture/turret-base-button";
                case TurretButton.TurretType.Aura:
                    return @"Texture/turret-aura-button";
                case TurretButton.TurretType.Missile:
                    return @"Texture/turret-missile-button-t";
            }
            return "";
        }
    }
}
