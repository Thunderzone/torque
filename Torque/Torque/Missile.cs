﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastHope
{
    class Missile : Projectile
    {
        const float AOE = 80.0f;
        private Vector2 impact;

        public Missile(int _damage, Enemy _target, Sprite _sprite, ProjectileMgr parent)
            : base(_damage, _target, _sprite, 50.0f, parent)
        {
            impact = new Vector2( _target.Position.X, _target.Position.Y );
        }

        protected override void onHit()
        {
            // deal damage in AOE
            List<Enemy> targets = target.waveMgr.getEnemiesInRange(impact, AOE);
            foreach( Enemy e in targets )
                e.dealDamage(damage);  

            // delete me please
            projectileMgr.destroyProjectile(this);

            projectileMgr.Mothership.explosionManager.AddExplosion(sprite.Center, new Vector2(1.0f, 1.0f));

            SoundManager.PlayExplosion(7);
        }

        protected override void calcVelocity()
        {
            //Calculate new velocity vector
            Vector2 targetLocation = impact;
            Vector2 newVelocity = targetLocation - sprite.Center;
            newVelocity.Normalize();
            newVelocity *= speed;

            //Update velocity vector
            sprite.Velocity = newVelocity;
        }

        protected override bool colision()
        {
            return Vector2.Distance(sprite.Center, impact) < 30;
        }
    }
}
